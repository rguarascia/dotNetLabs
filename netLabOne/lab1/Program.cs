﻿using System;
using System.IO;

//I, Ryan T Guarascia, 000379166 certify that this material is my original work. No other person's work has been used without due acknowledgement.

namespace lab1
{
    /// <summary>
    /// Main program. Handles the sorting and printing
    /// </summary>
    class Program
    {
        static Employee[] employees = new Employee[100];
        static bool run = true; // this acts as a flag for when the user wants to leave the program
        /// <summary>
        /// Reads in text file, display menus and makes sure the user entered a number
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Read();
            
            while (run)
            {
                string userInput = PrintMenu();
                int check;
                bool isNum = int.TryParse(userInput, out check);
                if (isNum)
                {
                    Sort(check);
                } else
                {
                    Console.WriteLine("Numbers only");
                }
            }
            
        }
        /// <summary>
        /// Reads in the employee CSV file and parses into an array for later usage
        /// </summary>
        static void Read()
        {
            string current;
            int counter = 0;
            StreamReader file = new StreamReader(@"employees.txt");

            while ((current = file.ReadLine()) != null)
            {
                string[] splitter = current.Split(',');
                employees[counter] = new Employee(splitter[0], Int32.Parse(splitter[1]), Decimal.Parse(splitter[2]), double.Parse(splitter[3]));
                counter++;
            }
        }
        /// <summary>
        /// Displays staff roaster 
        /// </summary>
        static void PrintStaff()
        {
            Console.WriteLine("{0, -15}  {1:D5}  {2,6:C}  {3:#0.00}  {4,9:C}", "NAME", "NUMBER", "   RATE", "    HOURS", "GROSS");
            foreach (Employee x in employees)
            {
                if (x != null)
                    x.PrintEmployee();
            }
        }
        /// <summary>
        /// Preforms various sorting methods based on what the user entered
        /// </summary>
        /// <param name="type">User selection</param>
        static void Sort(int type)
        {
            Console.Clear();
            for (int x = 0; x < employees.Length; x++) {
                for(int y = x++; y > 0; y--) {
                    if(employees[y] != null) {
                        switch (type)
                        {
                            case 1: // Name - Ascending
                                if(employees[y-1].GetName().CompareTo(employees[y].GetName()) > 0)
                                {
                                    var t = employees[y-1];
                                    employees[y-1] = employees[y];
                                    employees[y]= t;
                                }
                                break;
                            case 2: //Number Acending
                                if(employees[y-1].GetNumber().CompareTo(employees[y].GetNumber()) > 0)
                                {
                                    var t = employees[y-1];
                                    employees[y-1] = employees[y];
                                    employees[y]= t;
                                }
                                break;
                            case 3: //Rate Descending
                                if(employees[y-1].GetRate().CompareTo(employees[y].GetRate()) < 0)
                                {
                                    var t = employees[y-1];
                                    employees[y-1] = employees[y];
                                    employees[y]= t;
                                }
                                break;
                            case 4: //Hours Descending
                                if(employees[y-1].GetHours().CompareTo(employees[y].GetHours()) < 0)
                                {
                                    var t = employees[y-1];
                                    employees[y-1] = employees[y];
                                    employees[y]= t;
                                }
                                break;
                            case 5: //Gross Descending
                            if(employees[y-1].GetGross().CompareTo(employees[y].GetGross()) < 0)
                                {
                                    var t = employees[y-1];
                                    employees[y-1] = employees[y];
                                    employees[y]= t;
                                }
                                break;
                            case 6:
                                run = false;
                                break;
                            default:
                                Console.WriteLine("Invaild");
                            break;
                        }
                    }
                }
            }
            PrintStaff();
        }
        /// <summary>
        /// Printed the users menu for sort selection.
        /// </summary>
        /// <returns>The user selected sort</returns>
        static string PrintMenu()
        {
            Console.WriteLine("1. Sort by Employee Name (ascending)");
            Console.WriteLine("2  Sort by Employee Number (ascending)");
            Console.WriteLine("3  Sort by Employee Pay Rate (descending)");
            Console.WriteLine("4  Sort by Employee Hours (descending)");
            Console.WriteLine("5  Sort by Employee Gross Pay (descending)");
            Console.WriteLine("6  Exit");
            Console.Write("Enter Choice: ");
            return Console.ReadLine().ToString();
        }
    }
}
