﻿using System;

//I, Ryan T Guarascia, 000379166 certify that this material is my original work. No other person's work has been used without due acknowledgement.


namespace lab1
{
    /// <summary>
    /// Creates an Emlpoyee object
    /// </summary>
    class Employee
    {
        private String name;
        private int number;
        private Decimal rate;
        private Double hours;
        private Decimal gross;

        /// <summary>
        /// Employee constructor
        /// </summary>
        /// <param name="name"></param>
        /// <param name="number"></param>
        /// <param name="rate"></param>
        /// <param name="hours"></param>
        public Employee(string name, int number, Decimal rate, Double hours)
        {
            this.name = name;
            this.number = number;
            this.rate = rate;
            this.hours = hours;
            if (hours < 40.0)
            {
                gross = (decimal)hours * rate;
                return;
            }
            gross = (40.0M) * rate + ((decimal)hours - (40.0M)) * rate * (1.5M);
        }
        //Get methods
        public Decimal GetGross()
        {
            return gross;
        }
        public Double GetHours()
        {
            return hours;
        }
        public String GetName()
        {
            return name;
        }
        public int GetNumber()
        {
            return number;
        }
        public Decimal GetRate()
        {
            return rate;
        }
        public void SetHours(Double hours)
        {
            this.hours = hours;
        }
        public void SetName(String name)
        {
            this.name = name;
        }
        public void SetNumber(int number)
        {
            this.number = number;
        }
        public void SetRate(Decimal rate)
        {
            this.rate = rate;
        }
        public void PrintEmployee()
        {
            Console.WriteLine("*{0, -15} | {1:D5} | {2,6:C} | {3:#0.00} | {4,9:C}|", this.name, this.number, this.rate, this.hours, this.gross);
        }
    }
}
