﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace lab4b
{
    class Program
    {
        static void Main(string[] args)
        {
            readFile();
        }

        private static void readFile()
        {
            bool valid = true; //Constant check for valid tags
            string tab = ""; // Output tab
            Regex badTags = new Regex(@"\bhr\b|br|img"); // Container tags
            Stack<String> htmltags = new Stack<String>();
            string line;
            System.IO.StreamReader file = new System.IO.StreamReader("BadTest.html");
            while ((line = file.ReadLine()) != null && valid == true)
            {
                string[] parse = line.Split('<','>');
                //Go up by twos because of how the tags are parsed
                for(int x = 1; x < parse.Length; x += 2)
                {
                    parse[x] = parse[x].ToLower(); //Normalize things
                    //removes spaces
                    int index = parse[x].IndexOf(" ");
                    if (index > 0)
                        parse[x] = parse[x].Substring(0, index);

                    if (!badTags.Match(parse[x]).Success) //makes sure they are not nonContainer
                    {
                        if (!parse[x].Contains('/')) // Opening tab
                        {
                            Console.WriteLine(tab + "Found opening: " + parse[x]);
                            tab += "\t ";
                            htmltags.Push(parse[x]);
                        }
                        else // closing tag
                        {
                            if(parse[x].Trim('/').Trim() == htmltags.Pop())
                            {
                                if (tab.Length >= 2) //Removes from tab index
                                    tab = tab.Remove(tab.Length - 2, 2);
                                else
                                    tab = "";
                                Console.WriteLine(tab + "Found closing: " + parse[x].Trim('/'));                           
                            } else
                            {
                                valid = false;
                            }
                        }
                    }
                }
            }
            if(valid)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Tags are balanced properly");
            } else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Tags are not balanced properly");
            }
            //foreach (string x in htmltags)
            //{
            //    Console.WriteLine(x);
            //}
            Console.ReadKey();
        }
    
    }
}
