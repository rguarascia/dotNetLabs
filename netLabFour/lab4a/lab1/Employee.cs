﻿using System;
using System.Collections.Generic;

//I, Ryan T Guarascia, 000379166 certify that this material is my original work. No other person's work has been used without due acknowledgement.


namespace lab1
{
    /// <summary>
    /// Creates an Emlpoyee object
    /// </summary>
    class Employee : IComparable<Employee>
    {
        public String name;
        public int number;
        public Decimal rate;
        public Double hours;
        public Decimal gross;

        /// <summary>
        /// Employee constructor
        /// </summary>
        /// <param name="name"></param>
        /// <param name="number"></param>
        /// <param name="rate"></param>
        /// <param name="hours"></param>
        public Employee(string name, int number, Decimal rate, Double hours)
        {
            this.name = name;
            this.number = number;
            this.rate = rate;
            this.hours = hours;
            if (hours < 40.0)
            {
                gross = (decimal)hours * rate;
                return;
            }
            gross = (40.0M) * rate + ((decimal)hours - (40.0M)) * rate * (1.5M);
        }

        public int CompareTo(Employee other)
        {
            throw new NotImplementedException();
        }
        // static method to get a Comparer object
        public static EmployeeComparer GetComparer()
        {
            return new Employee.EmployeeComparer();
        }


        // Special implementation to be called by custom comparer
        public int CompareTo(Employee rhs, Employee.EmployeeComparer.ComparisonType Which)
        {
            switch (Which)
            {
                case Employee.EmployeeComparer.ComparisonType.EmpID:
                    return this.number.CompareTo(rhs.number);
                case Employee.EmployeeComparer.ComparisonType.EmpName:
                    return this.name.CompareTo(rhs.name);
                case Employee.EmployeeComparer.ComparisonType.EmpHours:
                    return this.hours.CompareTo(rhs.hours);
                case Employee.EmployeeComparer.ComparisonType.EmpGross:
                    return this.gross.CompareTo(rhs.gross);
                case Employee.EmployeeComparer.ComparisonType.EmpPay:
                    return this.rate.CompareTo(rhs.rate);
            }
            return 0;
        }
        // nested class which implements IComparer
        public class EmployeeComparer : IComparer<Employee>
        {
            // private state variable
            public Employee.EmployeeComparer.ComparisonType WhichComparison { get; set; }
            // enumeration of comparsion types
            public enum ComparisonType
            {
                EmpID,
                EmpName,
                EmpGross,
                EmpHours,
                EmpPay,

            };

            // Tell the Employee objects to compare themselves
            public int Compare(Employee lhs, Employee rhs)
            {
                return lhs.CompareTo(rhs, WhichComparison);
            }
        }

        public void PrintEmployee()
        {
            Console.WriteLine("*{0, -15} | {1:D5} | {2,6:C} | {3:#0.00} | {4,9:C}|", this.name, this.number, this.rate, this.hours, this.gross);
        }
    }
}
