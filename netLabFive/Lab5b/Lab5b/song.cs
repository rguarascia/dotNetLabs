﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5b
{
    /// <summary>
    /// new song object that inherits from Media
    /// </summary>
    class song : Media
    {
        /// <summary>
        /// Sets the constructor for the song object
        /// </summary>
        /// <param name="title">song title</param>
        /// <param name="year">song year</param>
        public song(string title, int year) : base(title, year)
        {
        }

        public string album { get; set; }
        public string artist { get; set; }
    }
}
