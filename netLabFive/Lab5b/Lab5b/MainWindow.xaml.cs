﻿using System;
using System.Windows;
using System.Windows.Input;
using System.IO;
//Ryan Guarascia
//April 6 2018
//I, Ryan T Guarascia, 000379166 certify that this material is my original work. No other person's work has been used without due acknowledgement.
namespace Lab5b
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Media[] myMedia = new Media[100]; // Media objects.
        public MainWindow()
        {
            InitializeComponent();
            Loaded += MainWindow_Load; //Subscribes to the onLoad event handler
        }
        /// <summary>
        /// onFormLoad subscription
        /// </summary>
        /// <param name="sender">Windows form sender object</param>
        /// <param name="e">Rout event args for the window form load</param>
        private void MainWindow_Load(object sender, RoutedEventArgs e)
        {
            try
            {
                using (StreamReader sr = File.OpenText("Data.txt"))
                    loadData(sr); //Load in the data to the media array
            }
            catch (IOException) //makes sure the file is present
            {
                MessageBox.Show("Please make sure that the Data.txt is placed in the same location as the EXE ");
                Environment.Exit(1);
            }
        }

        /// <summary>
        /// Takes the streamReader and loads the file to the Media array. Benifits of using the 'using' function and sending it to a function is that it closes and flushes the read by itself.
        /// </summary>
        /// <param name="sr">The Streamreader for the data.txt file that stores all the media</param>
        private void loadData(StreamReader sr)
        {
            string line = "";
            string thisType = "";
            int currentMedia = 0;
            while ((line = sr.ReadLine()) != null)
            {
                //checks if line is null
                if (line != "")
                {
                    string[] parseString = new string[100]; //Array of the string for each line. this will hold the basic information for the object.
                    switch (line.Substring(0, 5)) //checks the first 5 chars to see what data type it is. 
                    {
                        #region MOVIE
                        case "MOVIE":
                            thisType = "movie";
                            parseString = line.Split('|');
                            myMedia[currentMedia] = new movie(parseString[1], int.Parse(parseString[2]));
                            movie myMovie = myMedia[currentMedia] as movie;
                            myMovie.director = parseString[3];
                            break;
                        #endregion
                        #region BOOK
                        case "BOOK|":
                            thisType = "book";
                            parseString = line.Split('|');
                            myMedia[currentMedia] = new book(parseString[1], int.Parse(parseString[2]));
                            book myBook = myMedia[currentMedia] as book;
                            myBook.author = parseString[3];
                            break;
                        #endregion
                        #region SONG
                        case "SONG|":
                            parseString = line.Split('|');
                            myMedia[currentMedia] = new song(parseString[1], int.Parse(parseString[2]));
                            song mySong = myMedia[currentMedia] as song;
                            mySong.album = parseString[3];
                            mySong.artist = parseString[4];
                            break;
                        #endregion
                        #region NEXT
                        case "-----": //New media object found
                            currentMedia++;
                            break;
                        #endregion
                        #region SUMMARY
                        default: //Summary
                            if (thisType == "movie")
                            {
                                movie thisMovie = myMedia[currentMedia] as movie;
                                thisMovie.summary += line;
                            }
                            else if (thisType == "book")
                            {
                                book thisBook = myMedia[currentMedia] as book;
                                thisBook.summary += line + Environment.NewLine + Environment.NewLine;
                            }
                            break;
                            #endregion
                    } // End swtich
                } // End if
            } //End while 

        }

        /// <summary>
        /// Based on the user's input, adds the data to the listbox or to the textBlock if they searched
        /// </summary>
        /// <param name="type">States weither it is a book, movie, song, all or search element</param>
        /// <param name="input">the user's input for the seach function</param>
        private void addData(string type, string input)
        {
            bool search = false;
            //Checks if the user want to search, and that they entered in something to search
            if (type == "Search" && input != "")
            {
                search = true;
            }
            else if (input == "" && type == "Search")
            {
                MessageBox.Show("Please make sure you enter a title to search for first");
            } else
            {
                //if they don't search, then they must have pressed a button, therefore clear the listbox
                lstMedia.Items.Clear();
            }
            foreach (Media current in myMedia)
            {
                if (current != null)
                {
                    switch (current.GetType().ToString().Remove(0, 6)) //Changed this from lab3, to remove the namespace ext so it is universal
                    {
                        #region BOOK
                        case "book":
                            book thisBook = current as book;
                            if (type == "Book" || type == "All")
                            {
                                lstMedia.Items.Add(string.Format("Book Title: {0} ({1}) \nAuthor {2})", current.Title, current.Year, thisBook.author));
                            }
                            else if (search == true && thisBook.Search(input))
                            {
                                txtSummary.Text = thisBook.Decrypt();
                            }
                            break;
                        #endregion
                        #region MOVIE
                        case "movie":
                            movie thisMovie = current as movie;
                            if (type == "Movie" || type == "All")
                            {
                                lstMedia.Items.Add(string.Format("Movie Title: {0} ({1}) \nDirectory {2})", current.Title, current.Year, thisMovie.director));
                            }
                            else if (search == true && thisMovie.Search(input))
                            {
                                txtSummary.Text = thisMovie.Decrypt();
                            }
                            break;
                        #endregion
                        #region SONG
                        case "song":
                            song thisSong = current as song;
                            if (type == "Song" || type == "All")
                            {
                                lstMedia.Items.Add((string.Format("Song Title: {0} ({1}) \nAlbum: {2} | Artist {3}", current.Title, current.Year, thisSong.album, thisSong.artist)));
                            }
                            break;
                            #endregion
                    }// End switch
                }// End if
            }// End foreach
        }

        /// <summary>
        /// When the book button is pressed
        /// </summary>
        /// <param name="sender">button sender</param>
        /// <param name="e">routed event args</param>
        private void btnListBook_Click(object sender, RoutedEventArgs e)
        {
            addData("Book", txtSearch.Text);
        }

        /// <summary>
        /// When the movie button is pressed
        /// </summary>
        /// <param name="sender">button sender</param>
        /// <param name="e">routed event args</param>
        private void btnListMovies_Click(object sender, RoutedEventArgs e)
        {
            addData("Movie", txtSearch.Text);
        }

        /// <summary>
        /// When the song button is pressed
        /// </summary>
        /// <param name="sender">button sender</param>
        /// <param name="e">routed event args</param>
        private void btnListSongs_Click(object sender, RoutedEventArgs e)
        {
            addData("Song", txtSearch.Text);
        }

        /// <summary>
        /// When the all button is pressed
        /// </summary>
        /// <param name="sender">button sender</param>
        /// <param name="e">routed event args</param>
        private void btnListAll_Click(object sender, RoutedEventArgs e)
        {
            addData("All", txtSearch.Text);
        }

        /// <summary>
        /// When the search button is pressed
        /// </summary>
        /// <param name="sender">button sender</param>
        /// <param name="e">routed event args</param>
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            addData("Search", txtSearch.Text);
        }

        /// <summary>
        /// When a list item is selected
        /// </summary>
        /// <param name="sender">button sender</param>
        /// <param name="e">routed event args</param>
        private void lstMedia_Selected(object sender, RoutedEventArgs e)
        {
            txtSummary.Text = "";
            if (lstMedia.SelectedIndex != -1)
            {
                string title = lstMedia.SelectedValue.ToString().Split(':')[1].Split('(')[0].Trim(); //Parses the Listbox item to get the title of the media object to find the summary
                addData("Search", title);
            }
        }

        /// <summary>
        /// When you press enter
        /// </summary>
        /// <param name="sender">txtbox sender</param>
        /// <param name="e">key event arguments</param>
        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key.ToString() == "Return")
                addData("Search", txtSearch.Text);
        }
    }
}