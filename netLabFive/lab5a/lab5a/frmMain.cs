﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//Ryan Guarascia
//April 6 2018
//I, Ryan T Guarascia, 000379166 certify that this material is my original work. No other person's work has been used without due acknowledgement.

namespace lab5a
{
    /// <summary>
    /// Partial class for the Form
    /// </summary>
    public partial class frmMain : Form
    {
        //Graphic objects
        Color c = Color.LightBlue;
        private Graphics g;
        private Pen p;
        private SolidBrush b;
        //sets the values for the water and pouring graphics
        int x = 465;
        int size = 10;
        int pour = 10;

        /// <summary>
        /// frmMain load events
        /// </summary>
        public frmMain()
        {
            InitializeComponent();
            this.Paint += new PaintEventHandler(frmMain_Paint);
        }
        /// <summary>
        /// formPaint function
        /// </summary>
        /// <param name="sender">form sender</param>
        /// <param name="e">Paint Event Args</param>
        private void frmMain_Paint(object sender, PaintEventArgs e)
        {
            g = e.Graphics;
            p = new Pen(c);
            b = new SolidBrush(c);
            g = this.CreateGraphics();
            Pen box = new Pen(Color.LightBlue);
            g.DrawLine(box, 100, 465, 280, 465);
            g.DrawLine(box, 100, 465, 100, 287);
            g.DrawLine(box, 280, 465, 280, 287);
        }

        /// <summary>
        /// Closes the ap
        /// </summary>
        /// <param name="sender">form sender</param>
        /// <param name="e">Event args</param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        /// <summary>
        /// Opens Color Dialog and sets the colour
        /// </summary>
        /// <param name="sender">form sender</param>
        /// <param name="e">event args</param>
        private void btnColour_Click(object sender, EventArgs e)
        {
            ColorDialog dialog = new ColorDialog();
            dialog.AllowFullOpen = false;
            dialog.ShowHelp = true;
            dialog.Color = c;
            if (dialog.ShowDialog() == DialogResult.OK)
                c = dialog.Color;
        }

        /// <summary>
        /// When you sroll the trackbar set the timer val
        /// </summary>
        /// <param name="sender">form sender</param>
        /// <param name="e">event args</param>
        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            if (tbrControl.Value != 0)
            {
                timer1.Enabled = true;
                timer1.Interval = (11 - tbrControl.Value) * 100; //reverse the order of the trackbar and sets the interal of the timer
            }
            else
            {
                g.FillRectangle(new SolidBrush(Color.Black), new Rectangle(109, 200, 10, 250 - pour + 25)); // Paints over the pour black
                drawCurrentRectangle();
                if (timer1.Enabled)
                    timer1.Enabled = false;
            }
        }

        /// <summary>
        /// Per timer tick
        /// </summary>
        /// <param name="sender">form sender</param>
        /// <param name="e">event argsd</param>
        private void timer1_Tick(object sender, EventArgs e)
        {
            drawWaterPour(size);
            if (size != 45)
            {
                g = this.CreateGraphics();
                x -= size;
                drawCurrentRectangle();
                size += 5;
            }
        }

        /// <summary>
        /// Paints the current water square
        /// </summary>
        private void drawCurrentRectangle()
        {
            g.FillRectangle(b, new Rectangle(100, x, 180, size));
        }

        /// <summary>
        /// Paints the pour of water
        /// </summary>
        /// <param name="mySize"></param>
        private void drawWaterPour(int mySize)
        {
            if (mySize != 250)
            {
                g.FillRectangle(new SolidBrush(Color.Black), new Rectangle(109, 200, 10, 250 - pour + 25)); // Paints over the last 
                g.FillRectangle(b, new Rectangle(109, 200, 10, 250 - pour)); // Draws the current
                pour += 25; // moves the pour
            }
        }
    }
}
