﻿using System;
using System.Collections;
using System.IO;
using System.Text.RegularExpressions;

/* Author Ryan Guarascia
 * March 11 2018
 * lab 3. Read a txt full of media, parse the information and create different objects depending on their types. Decrypt the summary and create a menu
 * for users to chose differnt media object for to search titles.
 */

namespace lab3
{
    /// <summary>
    /// Main class that reads the TXT file, and creates correct objects depending on the media type.
    /// </summary>
    class Program
    { 

        public static Media[] myMedia = new Media[100];
        /// <summary>
        /// Reads and prints the data with a menu
        /// </summary>
        /// <param name="args">Main Args</param>
        static void Main(string[] args)
        {
            readData();
            printMenu();
            Console.ReadKey();
        }

        /// <summary>
        /// Prints the Menu for the user to interact with to pick their media type/search
        /// </summary>
        public static void printMenu()
        {
            string input = "";
            do
            {
                printData(input);
                if (input != "")
                {
                    Console.Write(Environment.NewLine + "Press any button to contiune...");
                    Console.ReadKey();
                    Console.Clear();
                }
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("Ryan's Media Collection!");
                Console.WriteLine("========================");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("1. List all books");
                Console.WriteLine("2. List all movies");
                Console.WriteLine("3. List all songs");
                Console.WriteLine("4. List all media");
                Console.WriteLine("5. Search all media by title");
                Console.WriteLine(Environment.NewLine + "6. Exit program");
                Console.Write("Enter Choice");

            } while ((input = Console.ReadLine()) != "6");
            Console.WriteLine("Goodbye");
        }

        /// <summary>
        /// Prints the data depending on what the user inputs. if the user enters 5, it prompts them for the media title that it will search for. 
        /// </summary>
        /// <param name="input">The user's select.</param>
        public static void printData(string input)
        {
            string title = null;
            if (input == "5")
            {
                Console.Write("Enter search string: ");
                title = Console.ReadLine();
            }
            foreach (Media current in myMedia)
            {
                if (current != null)
                {

                    switch (current.GetType().ToString())
                    {
                        case "lab3.book":
                            book thisBook = current as book;
                            if (input == "1" || input == "4")
                            {
                                Console.Write(Environment.NewLine);
                                Console.WriteLine(string.Format("Book Title: {0} ({1})", current.Title, current.Year));
                                Console.WriteLine(string.Format("Author: {0}", thisBook.author));
                                Console.WriteLine("----------------------");
                            }
                            else if (input == "5" && title != null && current.Search(title))
                            {
                                Console.Write(Environment.NewLine);
                                Console.WriteLine(string.Format("Book Title: {0} ({1})", current.Title, current.Year));
                                Console.WriteLine(string.Format("Author: {0}", thisBook.author));
                                Console.WriteLine(Environment.NewLine + thisBook.Decrypt());
                                Console.WriteLine("----------------------");
                            }
                            break;
                        case "lab3.movie":
                            movie thisMovie = current as movie;
                            if (input == "2" || input == "4")
                            {
                                Console.Write(Environment.NewLine);
                                Console.WriteLine(string.Format("Movie Title: {0} ({1})", current.Title, current.Year));
                                Console.WriteLine(string.Format("Director: {0}", thisMovie.director));
                                Console.WriteLine("----------------------");
                            }
                            else if (input == "5" && title != null && current.Search(title))
                            {
                                Console.Write(Environment.NewLine);
                                Console.WriteLine(string.Format("Book Title: {0} ({1})", current.Title, current.Year));
                                Console.WriteLine(string.Format("Director: {0}", thisMovie.director));
                                Console.WriteLine(Environment.NewLine + thisMovie.Decrypt());
                                Console.WriteLine("----------------------");
                            }
                            break;
                        case "lab3.song":
                            song thisSong = current as song;
                            if (input == "3" || input == "4")
                            {
                                Console.Write(Environment.NewLine);
                                Console.WriteLine(string.Format("Song Title: {0} ({1})", current.Title, current.Year));
                                Console.WriteLine(string.Format("Album: {0} | Artist: {1}", thisSong.album, thisSong.artist));
                                Console.WriteLine("----------------------");
                            }
                            else if (input == "5" && title != null && current.Search(title))
                            {
                                Console.Write(Environment.NewLine);
                                Console.WriteLine(string.Format("Song Title: {0} ({1})", current.Title, current.Year));
                                Console.WriteLine(string.Format("Album: {0} | Artist: {1}", thisSong.album, thisSong.artist));
                                Console.WriteLine("----------------------");
                            }
                            break;
                    }

                }
            }
        }

        /// <summary>
        /// Reads the text file into an array of Media objects depending on their type. We locate the type by the first 5 chars of the line.
        /// </summary>
        public static void readData()
        {
            int currentMedia = 0;
            string line;  
            System.IO.StreamReader file =
                new System.IO.StreamReader("Data.txt");
            string thisType = "";
            //while their is a line to read
            while ((line = file.ReadLine()) != null)
            {
                //checks if line is null
                if (line != "")
                {
                    string[] parseString = new string[100]; //Array of the string for each line. this will hold the basic information for the object.
                    switch (line.Substring(0, 5)) //checks the first 5 chars to see what data type it is. 
                    {
                        case "MOVIE":
                            thisType = "movie";
                            parseString = line.Split('|');
                            myMedia[currentMedia] = new movie(parseString[1], int.Parse(parseString[2]));
                            movie myMovie = myMedia[currentMedia] as movie;
                            myMovie.director = parseString[3];
                            break;
                        case "BOOK|":
                            thisType = "book";
                            parseString = line.Split('|');
                            myMedia[currentMedia] = new book(parseString[1], int.Parse(parseString[2]));
                            book myBook = myMedia[currentMedia] as book;
                            myBook.author = parseString[3];
                            break;
                        case "SONG|":
                            parseString = line.Split('|');
                            myMedia[currentMedia] = new song(parseString[1], int.Parse(parseString[2]));
                            song mySong = myMedia[currentMedia] as song;
                            mySong.album = parseString[3];
                            mySong.artist = parseString[4];
                            break;
                        case "-----": //New media object found
                            currentMedia++;
                            break;
                        default: //Summary
                            if (thisType == "movie")
                            {
                                movie thisMovie = myMedia[currentMedia] as movie;
                                thisMovie.summary += line;
                            }
                            else if (thisType == "book")
                            {
                                book thisBook = myMedia[currentMedia] as book;
                                thisBook.summary += line + Environment.NewLine + Environment.NewLine;
                            }
                            break;
                    }
                }
            }
            file.Close();
        }
    }
}
