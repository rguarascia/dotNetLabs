﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3
{
    /// <summary>
    /// new book object that inherits from Media and implements the Iencrytable interfaces
    /// </summary>
    class book : Media, IEncryptable
    {
        /// <summary>
        /// Constuctor for the book class
        /// </summary>
        /// <param name="title">movie title</param>
        /// <param name="year">movie year</param>
        public book(string title, int year) : base(title, year)
        {
        }

        public string author { get; set; }
        public string summary { get; set; }


        //Decrypt method taken from StackOverflow: https://stackoverflow.com/questions/18739091/is-it-possible-to-write-a-rot13-in-one-line
        /// <summary>
        /// Decrypts the summary of the book 
        /// </summary>
        /// <returns>the summary in plain english</returns>
        public string Decrypt()
        {
            if (string.IsNullOrEmpty(summary)) return summary;

            char[] buffer = new char[summary.Length];

            for (int i = 0; i < summary.Length; i++)
            {
                char c = summary[i];
                if (c >= 97 && c <= 122)
                {
                    int j = c + 13;
                    if (j > 122) j -= 26;
                    buffer[i] = (char)j;
                }
                else if (c >= 65 && c <= 90)
                {
                    int j = c + 13;
                    if (j > 90) j -= 26;
                    buffer[i] = (char)j;
                }
                else
                {
                    buffer[i] = (char)c;
                }
            }
            return new string(buffer);
        }

        /// <summary>
        /// Calls the Decrypt method that will encrypt the summary
        /// </summary>
        /// <returns>Encrypted summary</returns>
        public string Encrypt()
        {
            return Decrypt();
        }
    }
}
