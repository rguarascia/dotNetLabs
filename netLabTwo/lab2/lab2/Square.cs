﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    /// <summary>
    /// Square inheits rectangle
    /// </summary>
    class Square : Rectangle
    {
        /// <summary>
        /// sets the type to square
        /// </summary>
        public Square()
        {
            base.Type = "Square";
        }
        /// <summary>
        /// Sets the length. width=length
        /// </summary>
        public override void SetData()
        {
            Console.WriteLine("Length: ");
            base.Length = Convert.ToDouble(Console.ReadLine());
            base.Width = base.Length;
        }
        /// <summary>
        /// custom to string
        /// </summary>
        /// <returns>base,area,l,w</returns>
        public override string ToString()
        {
            return string.Format("{0:C11}{1,10:F2}{2:F2}{3:F2}", base.Type, "Area: " + this.CalculateArea(), " Length: " + this.Length, " Width: " + this.Width);
        }
    }
}
