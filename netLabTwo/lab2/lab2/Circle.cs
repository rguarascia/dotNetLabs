﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    /// <summary>
    /// Circle inherits from Ellipse
    /// </summary>
    class Circle : Ellipse
    {
        /// <summary>
        /// Circle constructor
        /// </summary>
        public Circle()
        {
            base.Type = "Circle";
        }

        /// <summary>
        /// sets semiMinor. Magor = Minor
        /// </summary>
        public override void SetData()
        {
            Console.WriteLine("Radius: ");
            base.semiMinor = Convert.ToDouble(Console.ReadLine());
            base.semiMajor = base.semiMinor;

        }

        /// <summary>
        /// Custom to string
        /// </summary>
        /// <returns>base,area,radius</returns>
        public override string ToString()
        {
            return string.Format("{0:C11}{1,10:F2}{2:F2}", base.Type, " Area: " + this.CalculateArea(), " Radius: " + base.semiMajor);

        }
    }
}
