﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    /// <summary>
    /// creates ellipse that inherits from 2dshapes
    /// </summary>
    class Ellipse : _2DShapes
    {
        /// <summary>
        /// Constructor for ellipse
        /// </summary>
        public Ellipse()
        {
            base.Type = "Ellipse";
            this.SetData();
        }
        /// <summary>
        /// get set for semiMinor
        /// </summary>
        public double semiMinor {get;set;}
        /// <summary>
        /// get set for semiMajor
        /// </summary>
        public double semiMajor { get; set; }

        /// <summary>
        /// pi * semiMinor * semiMajor
        /// </summary>
        /// <returns>area</returns>
        public override double CalculateArea()
        {
            return Shape.PI * this.semiMinor * this.semiMajor;
        }

        /// <summary>
        /// sets semiMinor and semiMajor
        /// </summary>
        public override void SetData()
        {
            Console.WriteLine("SemiMajor length: ");
            this.semiMajor = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("SemiMinor length: ");
            this.semiMinor = Convert.ToDouble(Console.ReadLine());

        }

        /// <summary>
        /// custom to string
        /// </summary>
        /// <returns>base,area,semiMajor and semiMinor</returns>
        public override string ToString()
        {
            return string.Format("{0:C11}{1,10:F2}{2:F2} s.major x {3:F2} s.minor", base.Type, this.CalculateArea(), this.semiMajor, this.semiMinor);

        }
    }
}
