﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    /// <summary>
    /// Cube inherits for box
    /// </summary>
    class Cube : Box
    {
        /// <summary>
        /// consturctor for cube
        /// </summary>
        public Cube()
        {
            base.Type = "Cube";
        }

        /// <summary>
        /// sets the length. width and height = length
        /// </summary>
        public override void SetData()
        {
            Console.WriteLine("Enter the length");
            this.Length = Convert.ToDouble(Console.ReadLine());
            this.Width = this.Length;
            this.Height = this.Length;
        }
    }
}
