﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    /// <summary>
    /// abstract class for all 3d shapes
    /// </summary>
    abstract class _3DShapes : Shape
    {
        /// <summary>
        /// constructor for 3d shapes
        /// </summary>
        public _3DShapes() { }
    }
}
