﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    /// <summary>
    /// 2DShape inherits from shape
    /// </summary>
    abstract class _2DShapes : Shape
    {
        /// <summary>
        /// Constructor for 2DShapes
        /// </summary>
        public _2DShapes() { }
        /// <summary>
        /// No volume in 2D shapes.
        /// </summary>
        /// <returns></returns>
        public override double CalculateVolume()
        {
            return 0.0;
        }
    }
}
