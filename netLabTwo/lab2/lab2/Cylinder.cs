﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    /// <summary>
    /// Cyinder object inherits 3dshape
    /// </summary>
    class Cylinder : _3DShapes
    {
        /// <summary>
        /// Constructor for Cylinder
        /// </summary>
        public Cylinder()
        {
            base.Type = "Cylinder";
            SetData();
        }
        /// <summary>
        /// get set for radius
        /// </summary>
        public double Radius { get; set; }
        /// <summary>
        /// get set for height
        /// </summary>
        public double Height { get; set; }

        /// <summary>
        /// pi * 2 * r * (r + h)
        /// </summary>
        /// <returnsarea></returns>
        public override double CalculateArea()
        {
            return (Shape.PI * 2) * this.Radius * (this.Radius + this.Height);
        }

        /// <summary>
        /// pi * r^2 * h
        /// </summary>
        /// <returns>volume</returns>
        public override double CalculateVolume()
        {
            return (Shape.PI * Math.Pow(this.Radius, 2) * this.Height);
        }

        /// <summary>
        /// custom to string
        /// </summary>
        /// <returns>base,area,volume,r,h</returns>
        public override string ToString()
        {
            return string.Format("{0:C11}{1,10:F2}{2:F2}{3:F2}{4:F2}", base.Type, " Area: " + this.CalculateArea(), " Volume: " + this.CalculateVolume(), " Radius: " + this.Radius, " Height: " + this.Height);
        }
        /// <summary>
        /// sets radius and height
        /// </summary>
        public override void SetData()
        {
            Console.WriteLine("Radius: ");
            this.Radius = double.Parse(Console.ReadLine());
            Console.WriteLine("Height: ");
            this.Height = double.Parse(Console.ReadLine());
        }
    }
}
