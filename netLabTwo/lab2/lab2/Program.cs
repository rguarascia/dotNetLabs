﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    class Program
    {
        /// <summary>
        /// Creates an array of 100 shapes
        /// </summary>
        static public Shape[] myShapes = new Shape[100];
        static int count = 0;
        static void Main(string[] args)
        {
            string user = "";
            //while there are no more than 100 shapes
            while (count != 99)
            {
                //0 prints the results
                while ((user = printMenu()) != "0")
                {
                    {
                        switch (user.ToUpper())
                        {

                            case "A":
                                myShapes[count] = new Rectangle();
                                break;
                            case "B":
                                myShapes[count] = new Square();
                                break;
                            case "C":
                                myShapes[count] = new Cube();
                                break;
                            case "D":
                                myShapes[count] = new Box();
                                break;
                            case "E":
                                myShapes[count] = new Ellipse();
                                break;
                            case "F":
                                myShapes[count] = new Circle();
                                break;
                            case "G":
                                myShapes[count] = new Cylinder();
                                break;
                            case "H":
                                myShapes[count] = new Sphere();
                                break;
                            case "I":
                                myShapes[count] = new Triangle();
                                break;
                            case "J":
                                myShapes[count] = new Tetrahedron();
                                break;
                            default:
                                Console.WriteLine("Incorrect submission. Please enter a vaild letter");
                                count--;
                                break;
                        }
                        count++;
                    }
                }
                //after the enter 0, display the result
                printResults();
                Console.ReadLine();
                break;
            }
        }

        /// <summary>
        /// illiterates through the shapes array and prints out the results
        /// </summary>
        static void printResults()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Cyan;
            for(int x = 0; x < count; x++)
            {
                Console.WriteLine("#" + (x+1) + ": " +  myShapes[x].ToString());
            }
        }

        /// <summary>
        /// creates menu
        /// </summary>
        /// <returns>menu</returns>
        static string printMenu()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Ryan's Geometry Class:");
            Console.ResetColor();
            Console.WriteLine("*A - Rectangle       *E - Ellipse        *I - Triangle");
            Console.WriteLine("*B - Square          *F - Circle         *J - Tetrahedron");
            Console.WriteLine("*C - Box             *G - Cylinder");
            Console.WriteLine("*D - Cube            *H - Sphere");
            Console.WriteLine("\n0 - List all shapes and Exit...");
            return Console.ReadLine().ToString();
        }
    }
}
