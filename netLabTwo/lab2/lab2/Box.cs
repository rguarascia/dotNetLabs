﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{

    /// <summary>
    /// box shape object inheits from 3d shape
    /// </summary>
    class Box : _3DShapes
    {
        /// <summary>
        /// Sets type and calls set data
        /// </summary>
        public Box()
        {
            base.Type = "Box";
            this.SetData();
        }
        /// <summary>
        /// Box length
        /// </summary>
        public double Length { get; set; }
        /// <summary>
        /// Box Width
        /// </summary>
        public double Width { get; set; }
        /// <summary>
        /// Box Height
        /// </summary>
        public double Height { get; set; }

        /// <summary>
        /// L * W * H
        /// </summary>
        /// <returns>Volume</returns>
        public override double CalculateVolume()
        {
            return this.Length * this.Width * this.Height;

        }

        /// <summary>
        /// (L*W) + (L*H) + (W*H) * 2
        /// </summary>
        /// <returns>Areas</returns>
        public override double CalculateArea()
        {
            return ((this.Length * this.Width) + (this.Length * this.Height) + (this.Width * this.Height)) * 2.0;

        }
        /// <summary>
        /// custom to string
        /// </summary>
        /// <returns>area, volume, l, w </returns>
        public override string ToString()
        {
            return string.Format("{0:C11}{1,10:F2}{2:F2}{3:F2}{4:F2}{5:F2}", base.Type, "Area: " + this.CalculateArea(), " Volume: " + this.CalculateVolume(), " Length: " + this.Length, " Width: " + this.Width, " Height: " + this.Height);
        }
        /// <summary>
        /// Sets L W H 
        /// </summary>
        public override void SetData()
        {
            Console.WriteLine("Length: ");
            this.Length = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Width: ");
            this.Width = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Height: ");
            this.Height = Convert.ToDouble(Console.ReadLine());

        }
    }
}
