﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    /// <summary>
    /// Sphere ofject that inhiets from 3DShapes
    /// </summary>
    class Sphere : _3DShapes
    {
        /// <summary>
        /// Get set for the radius
        /// </summary>
        public int Radius { get; set; }

        /// <summary>
        /// constructor that set base type to sphere
        /// </summary>
        public Sphere()
        {
            base.Type = "Sphere";
            SetData();
        }

        /// <summary>
        /// pi * r^2 * 4
        /// </summary>
        /// <returns>Area</returns>
        public override double CalculateArea()
        {
            return  Shape.PI * Math.Pow(this.Radius, 2) * 4;
        }

        /// <summary>
        /// pi/3 * radius^3 * 4
        /// </summary>
        /// <returns>volume</returns>
        public override double CalculateVolume()
        {
            return (Shape.PI/3) * Math.Pow(this.Radius,3) * 4;
        }

        /// <summary>
        /// Gets radius
        /// </summary>
        public override void SetData()
        {
            Console.WriteLine("Radius: ");
            this.Radius = int.Parse(Console.ReadLine());
        }

        /// <summary>
        /// overrides custom to string
        /// </summary>
        /// <returns>base,area,volume,radius</returns>
        public override string ToString()
        {
            return string.Format("{0:C11}{1,10:F2}{2:F2}{3:F2}", base.Type, "Area: " + this.CalculateArea(), " Volume: " + this.CalculateVolume(), " Radius: " + this.Radius);

        }
    }
}
