﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    /// <summary>
    /// Tetrahedron Class
    /// </summary>
    class Tetrahedron : _3DShapes
    {
        /// <summary>
        /// get set for length
        /// </summary>
        public double Length { get; set; }
        /// <summary>
        /// sets the type and sets the data
        /// </summary>
        public Tetrahedron()
        {
            base.Type = "Tetragedron";
            SetData();
        }

        /// <summary>
        /// sqrt 3 * length^2
        /// </summary>
        /// <returns>area</returns>
        public override double CalculateArea()
        {
            return Math.Sqrt(3.0) * Math.Pow(this.Length,2);

        }

        /// <summary>
        /// sqrt2/12 * length^3
        /// </summary>
        /// <returns>volume</returns>
        public override double CalculateVolume()
        {
            return Math.Sqrt(2.0) / 12.0 * Math.Pow(this.Length,3);
        }

        /// <summary>
        /// sets the length
        /// </summary>
        public override void SetData()
        {
            Console.WriteLine("Length:");
            this.Length = double.Parse(Console.ReadLine());
        }

        /// <summary>
        /// custom to string
        /// </summary>
        /// <returns>base,area,volume,l</returns>
        public override string ToString()
        {
            return string.Format("{0:C11}{1,10:F2}{2:F2}{3:F2}", base.Type, " Area: " + this.CalculateArea(), " Volume: " + this.CalculateVolume(), " length: " + this.Length);
        }
    }
}
