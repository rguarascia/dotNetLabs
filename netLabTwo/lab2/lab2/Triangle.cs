﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    /// <summary>
    /// Triangle inherits 2dShapes
    /// </summary>
    class Triangle : _2DShapes
    {
        /// <summary>
        /// Constructor for Triangle
        /// </summary>
        public Triangle ()
        {
            base.Type = "Triangle";
            this.SetData();
        }
        /// <summary>
        /// get set for the triangle base
        /// </summary>
        public double triBase { get; set; }
        /// <summary>
        /// get set for height
        /// </summary>
        public double height { get; set; }

        /// <summary>
        /// base/2 * height
        /// </summary>
        /// <returns>area</returns>
        public override double CalculateArea()
        {
            return (this.triBase/2) * this.height;
        }

        /// <summary>
        /// sets base and height
        /// </summary>
        public override void SetData()
        {
            Console.WriteLine("Base: ");
            this.triBase = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Height: ");
            this.height = Convert.ToDouble(Console.ReadLine());
        }

        /// <summary>
        /// custom to strig
        /// </summary>
        /// <returns>base,area,base,height</returns>
        public override string ToString()
        {
            return string.Format("{0:C11}{1,10:F2}{2:F2}{3:F2}", base.Type, " Area: " + this.CalculateArea(), " TriBase: " + this.triBase, " Height: " + this.height);
        }
    }
}
