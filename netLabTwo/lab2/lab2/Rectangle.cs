﻿using System;


namespace lab2
{
    /// <summary>
    /// rectangle inherits from 2dshape
    /// </summary>
    class Rectangle : _2DShapes
    {
        /// <summary>
        /// get set for width
        /// </summary>
        public double Width { get; set; }
        /// <summary>
        /// get set for length
        /// </summary>
        public double Length { get; set; }
        /// <summary>
        /// constructor for rectangle
        /// </summary>
        public Rectangle()
        {
            base.Type = "Rectangle";
            SetData();
        }

        /// <summary>
        /// W* L
        /// </summary>
        /// <returns>area</returns>
        public override double CalculateArea()
        {
            return this.Width * this.Length;
        }

        /// <summary>
        /// sets the length and width
        /// </summary>
        public override void SetData()
        {
            Console.WriteLine("Length: ");
            this.Length = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Width: ");
            this.Width = Convert.ToDouble(Console.ReadLine());
        }

        /// <summary>
        /// custom to string
        /// </summary>
        /// <returns>base,area,l,w</returns>
        public override string ToString()
        {
            return string.Format("{0:C11}{1,10:F2}{2:F2}{3:F2}", base.Type, "Area: " + this.CalculateArea(), " Length: " + this.Length, " Width: " + this.Width);
        }
    }
}
